#####
# Terraform protection policy
#####

output "iam_policy_terraform_protection_id" {
  value = element(concat(aws_iam_policy.terraform_protection.*.id, [null]), 0)
}

output "iam_policy_terraform_protection_arn" {
  value = element(concat(aws_iam_policy.terraform_protection.*.arn, [null]), 0)
}

output "iam_policy_policy_terraform_protection_id" {
  value = element(concat(aws_iam_policy.terraform_protection.*.policy_id, [null]), 0)
}

output "iam_policy_terraform_protection_path" {
  value = element(concat(aws_iam_policy.terraform_protection.*.path, [null]), 0)
}
